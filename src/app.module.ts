import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/articles.module';
import { MongooseModule } from '@nestjs/mongoose/dist';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ArticlesModule,
    ConfigModule.forRoot(),
   MongooseModule.forRoot('mongodb://localhost/experts')
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
