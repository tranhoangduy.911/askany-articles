/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
export type ArticlesDocument = LogOrder & mongoose.Document;
// import { Owner } from '../owners/schemas/owner.schema';

@Schema({ collection: 'articles' })
export class LogOrder {
    @Prop({
        type: mongoose.SchemaTypes.ObjectId,
        default: () => new mongoose.Types.ObjectId(),
    })
    _id: string;

    @Prop({ type: String, require: false, trim: true })
    name: string;

    @Prop({type: String, require: false, trim: true})
	content: string;

    @Prop({ type: String, require: false, trim: true })
	description: string;

	/*** STATUS
	 * 2 - public, index
	 * 1 - public, noindex
	 * 0 - private
	 * -1 - trash
	 ***/
    @Prop({ type: Number, default: 0, require: false })
	status: String;

    // default
    @Prop({ type: Date, default: mongoose.now(), required: false })
    createdAt: Date;

    @Prop({ type: Date, default: mongoose.now(), required: false })
    modifiedAt: Date;
}

const ArticleSchema = SchemaFactory.createForClass(LogOrder);
// ArticleSchema.index({
//   province: 'text',
// });
export { ArticleSchema };
