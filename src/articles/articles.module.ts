import { Module } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { ArticlesController } from './articles.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';
import { ArticleSchema } from './schemas/articles.schemas';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'articles', schema: ArticleSchema }]),
    HttpModule,
  ],
  providers: [ArticlesService],
  controllers: [ArticlesController]  
})
export class ArticlesModule {}
