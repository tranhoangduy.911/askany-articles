import {
    Body,
    ConsoleLogger,
    Controller,
    Get,
    Post,
    Req,
    Res,
} from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { ArticlesService } from './articles.service';
@Controller('articles')
export class ArticlesController {
    constructor(private readonly ArticlesService: ArticlesService) { }
    @MessagePattern('getAllArticles')
    handleGetList(data) {
        const signalGetData = this.ArticlesService.handleGetAll(data);
        return signalGetData;
    }
}
