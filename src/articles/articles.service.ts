import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ArticleSchema, ArticlesDocument} from './schemas/articles.schemas';
import { HttpService } from '@nestjs/axios';
@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel('articles')
    protected readonly MODEL: Model<ArticlesDocument>,
    private readonly httpService: HttpService,
  ) {
    // super(MODEL);
  }
  async handleGetAll(obj) {
    const signalGet = await this.MODEL.find({});
    return { message: 'PHƯƠNG YÊU THƯƠNG', data: signalGet };
  }
}

